<?php
/**
 * Created by PhpStorm.
 * User: Avell
 * Date: 02/12/2018
 * Time: 01:39
 */

namespace App\Controllers;

use App\Models\User;
use Core\Session;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Core\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;


class MailerController extends BaseController
{
    private $user;

    public function __construct(Response $response, Request $request, Environment $twig)
    {
        parent::__construct($response, $request, $twig);
        $this->mail = new PHPMailer(false);
        $this->user = new User();
    }

    public function getMail(){
        $this->setPageTitle("Forget Password");
        return $this->renderView("/mailer/send", "base");
    }

    public function sendMail(){

        $pass = $this->getPassword();
        if (isset($pass)){
            try {
                //Server settings
                //$this->mail->SMTPDebug = 2;                                 // Enable verbose debug output
                $this->mail->isSMTP();                                      // Set mailer to use SMTP
                $this->mail->Host = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
                $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
                $this->mail->Username = 'apikey';                 // SMTP username
                $this->mail->Password = "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";                           // SMTP password
                $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $this->mail->Port = 25;                                    // TCP port to connect to

                //Recipients
                $this->mail->setFrom('jplinkinpark@gmail.com', 'Mailer');
                $this->mail->addAddress($this->request->get('email'));     // Add a recipient

                //Content
                $this->mail->isHTML(true);                                  // Set email format to HTML
                $this->mail->Subject = 'Pass Recover';
                $this->mail->Body    = 'Senha: '.$pass;

                $this->mail->send();

                echo '<script>window.location = "/login"</script>';
            } catch (Exception $e) {
                Session::set('error',$this->mail->ErrorInfo);

                echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                    echo Session::get('error') . '<br>';
                echo "<i class=\"glyphicon glyphicon-ok-alert\"></i>";
                echo "</div>";

                Session::destroy('error');
            }
        }else{
            $this->view['error'] = Session::get('error');
            Session::destroy('error');

            echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
            foreach ($this->view['error'] as $key => $value) {
                echo $value . '<br>';
            }
            echo "<i class=\"glyphicon glyphicon-ok-alert\"></i>";
            echo "</div>";
        }

    }

    public function getPassword(){
        $this->user = User::where('email', $this->request->get('email'))->first();

        if(isset($this->user)){
            $password = $this->user->password;
            Session::set('sucess', ["Senha enviada com sucesso"]);
            return $password;
        }else{
            Session::set('error', ["E-mail não cadastrado"]);
            return null;
        }
    }
}