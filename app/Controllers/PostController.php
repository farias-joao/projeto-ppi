<?php

namespace App\Controllers;

use App\Models\Category;
use App\Models\Post;
use Core\BaseController;
use Core\Redirect;
use Core\Session;
use Core\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Core\Auth;


class PostController extends BaseController
{
    private $post;

    public function __construct(Response $response, Request $request, Environment $twig)
    {
        parent::__construct($response, $request, $twig);
        $this->post = new Post();
    }

    public function show($id)
    {
        $this->view['posts'] = $this->post->find($id);
        $this->view['category'] = Category::all();

        $this->renderView('/posts/show', 'base');


    }

    public function index()
    {
        $this->setPageTitle("Posts");
        $this->view['posts'] = $this->post->All();

        return $this->renderView("posts/index", 'base');
    }

    public function create()
    {
        if (Auth::check()) {
            $this->setPageTitle("New Post");
            $this->view['category'] = Category::all();

            return $this->renderView("/posts/create", "base");
        } else {
            return Redirect::route("/login", ['error' => ['Login necessário']]);
        }
    }

    public function store()
    {
        $data = [
            'title' => $this->request->get('title'),
            'content' => $this->request->get('content'),
            'user_id' => Auth::id()
        ];


        try {
            if (!Validator::make($data, $this->post->rules())) {
                $post = $this->post->create($data);
                if ($this->request->get('category_id') != null) {
                    $post->category()->attach($this->request->get('category_id'));
                }
                Session::set("sucess", ['Post Criado com Sucesso!']);

                echo '<script>window.location = "/posts"</script>';
            } else {
                $this->view['error'] = Session::get('error');
                Session::destroy('error');

                echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                foreach ($this->view['error'] as $key => $value) {
                    echo $value . '<br>';
                    echo "<i class=\"glyphicon glyphicon-ok-alert\"></i>";
                }
                echo "</div>";
            }
        } catch (\Exception $e) {
            echo "0";
        }
    }

    public function edit($id)
    {
        if (Post::where('id', $id)->exists() != null) {
            $this->view['post'] = $this->post->find($id);
            $this->view['title'] = "Edit Post";

            if ($this->validatorActions($this->view['post'])) {
                $this->view['category'] = Category::all();
                return $this->renderView("/posts/edit", "base");

            } else {
                Redirect::route('/posts', ['error' => ['Post nao pertence a ' . Auth::name()]]);
            }
        } else {
            Redirect::route('/posts', ['error' => ['Post nao existente']]);
        }
    }

    public function validatorActions($post_id)
    {
        if (Auth::id() != $post_id->user->id) {
            return false;
        } else {
            return true;
        }
    }

    public function update($id)
    {
        $data = [
            'title' => $this->request->get('title'),
            'content' => $this->request->get('content'),
        ];

        try {
            if (!Validator::make($data, $this->post->rules())) {
                $post = $this->post->find($id);
                $post->update($data);

                if ($this->request->get('category_id') != null) {
                    $post->category()->sync($this->request->get('category_id'));
                } else {
                    post()->detach();
                }

                Session::set('sucess' ,['Post Atualizado com sucesso']);
                echo '<script>window.location = "/posts"</script>';
            }else{
                $this->view['error'] = Session::get('error');
                Session::destroy('error');

                echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                foreach ($this->view['error'] as $key => $value) {
                    echo $value . '<br>';
                }
                echo "<i class=\"glyphicon glyphicon-ok-alert\"></i>";
                echo "</div>";
            }
        } catch (\Exception $e) {
            Session::set('error',$e->getMessage());

            echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                echo Session::get('error'). '<br>';
                echo "<i class=\"glyphicon glyphicon-ok-alert\"></i>";
            echo "</div>";

            Session::destroy('error');
        }
    }

    public function delete($id)
    {
        try {
            if (Post::where('id', $id)->exists() != null) {
                if (Auth::check()) {
                    if ($this->validatorActions($this->post->find($id))) {
                        $this->post->find($id)->delete();
                        Redirect::route('/posts',['sucess' => ['Post removido ']]);
                    }else{
                        Redirect::route('/posts', ['error' => ['Post nao pertence a ' . Auth::name()]]);
                    }
                }else{
                    Redirect::route('/login',['error' => ['Login necessário']]);
                }
            } else {
                Redirect::route('/posts',['error' => ['Post Inválido']]);
            }
        } catch (\Exception $e) {
            echo 'erro';
        }
    }
}