<?php

namespace App\Controllers;

use App\Models\User;
use Core\Authenticate;
use Core\BaseController;
use Core\Session;
use Core\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class UserController extends BaseController
{
    use Authenticate;
    private $user;

    public function __construct(Response $response, Request $request, Environment $twig)
    {
        parent::__construct($response, $request, $twig);
        $this->user = new User();
    }

    public function create()
    {
        $this->setPageTitle("New User");
        return $this->renderView("/user/create", "base");
    }

    public function store()
    {
        $data = [
            'name' => $this->request->get('name'),
            'email' => $this->request->get('email'),
            'password' => $this->request->get('password')
        ];

        try {
            if (!Validator::make($data, $this->user->rulesCreate())) {
                $data['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);
                $this->user->create($data);

                $this->auth();

                echo '<script>window.location = "/"</script>';
            } else {
                $this->view['error'] = Session::get('error');
                Session::destroy('error');
                echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                foreach ($this->view['error'] as $key => $value){
                    echo "<i class=\"glyphicon glyphicon-ok-alert\"></i>";
                    echo $value.'<br>';
                }
                echo "</div>";
            }
        } catch (\Exception $e) {
            echo 'erro';
        }
    }
}