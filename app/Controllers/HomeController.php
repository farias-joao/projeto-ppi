<?php

namespace App\Controllers;

use Core\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class HomeController extends BaseController
{


    public function __construct(Response $response, Request $request, Environment $twig)
    {
        parent::__construct($response, $request, $twig);
    }

    public function index()
    {
        $this->setPageTitle("Home");
        return $this->renderView('/home/index', 'base');

    }
}