<?php
/**
 * Created by PhpStorm.
 * User: Avell
 * Date: 25/11/2018
 * Time: 19:11
 */

namespace App\Models;


use Core\BaseModelEloquent;

class Category extends BaseModelEloquent
{
    public $table = 'categories';
    public $timestamps = false;

    protected $fillable = ['name','description'];

    public function post(){
        return $this->belongsToMany(Post::class);
    }
}