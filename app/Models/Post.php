<?php
/**
 * Created by PhpStorm.
 * User: Avell
 * Date: 25/11/2018
 * Time: 16:19
 */

namespace App\Models;


use Core\BaseModelEloquent;

class Post extends BaseModelEloquent
{
    public $table = "posts";
    public $timestamps = false;

    protected $fillable = ['title','content','user_id'];

    public function rules(){
        return  [
            'title' => 'min:2|max:10',
            'content' => 'min:2'
        ];
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsToMany(Category::class);
    }
}