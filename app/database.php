<?php

return [
    /*
     * Options(mysql,sqlite)
     * */
    'driver' =>'sqlite',
    'sqlite' => [
        'database' => 'database.db'
    ],
    'mysql' => [
        'host' => 'localhost',
        'database' => 'db_teste_projeto',
        'user' => 'joao',
        'pass' => '123',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci'
    ]

];