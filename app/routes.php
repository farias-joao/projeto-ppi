<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$routes = new RouteCollection();

$routes->add('page_index', new Route('/',
    array('_controller' => 'App\Controllers\HomeController','method' => 'index', 'suffix' => '')));

$routes->add('page_posts_index', new Route('/posts',
    array('_controller' => 'App\Controllers\PostController','method' => 'index', 'suffix' => '')));
$routes->add('page_posts_show', new Route('/posts/{suffix}/show',
    array('_controller' => 'App\Controllers\PostController','method' => 'show', 'suffix' => '')));
$routes->add('page_posts_create', new Route('/posts/create',
    array('_controller' => 'App\Controllers\PostController','method' => 'create', 'suffix' => '')));
$routes->add('page_posts_store', new Route('/posts/store',
    array('_controller' => 'App\Controllers\PostController','method' => 'store', 'suffix' => '')));
$routes->add('page_posts_edit', new Route('/posts/edit/{suffix}',
    array('_controller' => 'App\Controllers\PostController','method' => 'edit', 'suffix' => '')));
$routes->add('page_posts_update', new Route('/posts/update/{suffix}',
    array('_controller' => 'App\Controllers\PostController','method' => 'update', 'suffix' => 'id')));
$routes->add('page_posts_delete', new Route('/posts/{suffix}/delete',
    array('_controller' => 'App\Controllers\PostController','method' => 'delete', 'suffix' => 'id')));


$routes->add('page_users_login', new Route('/login',
    array('_controller' => 'App\Controllers\UserController','method' => 'login', 'suffix' => '')));
$routes->add('page_users_create', new Route('/user/create',
    array('_controller' => 'App\Controllers\UserController','method' => 'create', 'suffix' => '')));
$routes->add('page_users_store', new Route('/user/store',
    array('_controller' => 'App\Controllers\UserController','method' => 'store', 'suffix' => '')));

$routes->add('page_users_authenticate', new Route('/user/auth',
    array('_controller' => 'App\Controllers\UserController','method' => 'auth', 'suffix' => '')));
$routes->add('page_users_logout', new Route('/user/logout',
    array('_controller' => 'App\Controllers\UserController','method' => 'logout', 'suffix' => '')));

$routes->add('page_user_forget', new Route('/user/forget',
    array('_controller' => 'App\Controllers\MailerController','method' => 'getMail', 'suffix' => '')));
$routes->add('page_user_send', new Route('/user/forget/send',
    array('_controller' => 'App\Controllers\MailerController','method' => 'sendMail', 'suffix' => '')));

return $routes;