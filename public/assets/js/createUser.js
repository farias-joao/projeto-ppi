$(function () {
    $('#form_create_post').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: '/posts/store',
            data: $('#form_create_post').serialize(),
            success: function (resp) {
                $('#error').html(resp);
                $("#error").css({display: "inline"});
                setTimeout(function () {
                    $("#error").css({display: "none"});
                }, 5000);
            }
        });
    });
})
;

$(function () {
    $('#form_edit_post').submit(function (e) {
        e.preventDefault();

        var txt = $('#form_edit_post').serialize();
        var post_id = document.getElementById("id_post").getAttribute('value');

        $.ajax({
            type: 'post',
            url: '/posts/update/' + post_id,
            data: txt,
            success: function (resp) {
                $('#error').html(resp);
                $("#error").css({display: "inline"});
                setTimeout(function () {
                    $("#error").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#form_login').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/user/auth',
            data: $('#form_login').serialize(),
            success: function (resp) {
                $("#error").css({display: "inline"});
                $("#error").html(resp);
                setTimeout(function () {
                    $("#error").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#form_create_user').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: '/user/store',
            data: $('#form_create_user').serialize(),
            datatype: 'json',
            success: function (resp) {
                $("#error").html(resp);
                $("#error").css({display: "inline"});
                setTimeout(function () {
                    $("#error").css({display: "none"});
                }, 5000);
            }
        });
    });
});


$(function () {
    $('#link_logout').click(function (e) {

        $.ajax({
            type: 'post',
            url: '/user/logout',
            success: function (resp) {

            }
        });
    });
});

$(document).ready(function () {
    $('#form_user_forget').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: '/user/forget/send',
            data: $('#form_user_forget').serialize(),
            datatype: 'json',
            success: function (resp) {
                $("#error").html(resp);
                $("#error").css({display: "inline"});
                setTimeout(function () {
                    $("#error").css({display: "none"});
                }, 5000);
            }
        });
    });
});