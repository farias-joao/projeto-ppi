<?php
if (!session_id()) {
    session_start();
}

require __DIR__ ."/vendor/autoload.php";
include __DIR__ ."/app/routes.php";

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

$loader = new FilesystemLoader(__DIR__ ."/app/Views");
$twig = new Environment($loader);

$request = Request::createFromGlobals();

$context = new RequestContext();
$context->fromRequest($request);

$response = Response::create();

$matcher = new UrlMatcher($routes, $context);

try {
    $atributos = $matcher->match($context->getPathInfo());

    $controller = $atributos['_controller'];
    $method = $atributos['method'];
    if (isset($atributos['suffix']))
        $parametros = $atributos['suffix'];
    else
        $parametros = '';
    $obj = new $controller($response, $request, $twig);
    $obj->$method($parametros);

} catch (Exception $ex) {
    $response->setContent('Not found fde', Response::HTTP_NOT_FOUND);
}

$response->send();