<?php

namespace Core;

use Symfony\Component\HttpFoundation\RedirectResponse;

class Redirect
{
    private $response;
    public static function route($url, $with = [])
    {
        if(count($with) > 0 ){
            foreach ($with as $key => $value){
                Session::set($key, $value);
            }
        }

        $response = new RedirectResponse($url);
        return $response->send();
    }
}