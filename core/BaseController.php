<?php
/**
 * Created by PhpStorm.
 * User: Avell
 * Date: 24/11/2018
 * Time: 16:22
 */

namespace Core;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

abstract class BaseController
{
    protected $view;
    protected $inputs;
    protected $request;
    protected $response;
    private $viewPath;
    private $layoutPath;
    private $pageTitle = null;
    private $twig;

    public function __construct(Response $response, Request $request, Environment $twig)
    {
        $this->view = [];
        $this->view['auth'] = new Auth();

        if (Session::get('error')) {
            $this->view['error'] = Session::get('error');
            Session::destroy('error');
        }
        if (Session::get('inputs')) {
            $this->view['inputs'] = Session::get('inputs');
            Session::destroy('inputs');
        }
        if (Session::get('sucess')) {
            $this->view['sucess'] = Session::get('sucess');
            Session::destroy('sucess');
        }
        $this->request = $request;
        $this->response = $response;
        $this->twig = $twig;
    }

    public function forbiden()
    {
        return Redirect::route('/login');
    }

    protected function renderView($viewPath, $layoutPath = null)
    {
        $this->viewPath = $viewPath;
        $this->layoutPath = $layoutPath;

        if (isset($layoutPath)) {
            return $this->layout();
        } else {
            return $this->content();
        }
    }

    protected function layout()
    {
        $this->view['layout'] = "{$this->layoutPath}.twig";

        $this->content();

    }

    protected function content()
    {
        $this->response->setContent($this->twig->render(("{$this->viewPath}.twig"), $this->view));
        $this->setTimeout();
    }

    protected function setTimeout()
    {
        if (isset($this->view['error'])) {
            echo '<script>
        setTimeout(function () {
            $("#error").css({display: "none"});
                    }, 5000);
        </script>';
        } elseif (isset($this->view['sucess'])) {
            echo '<script>
        setTimeout(function () {
            $("#sucess").css({display: "none"});
                    }, 5000);
        </script>';
        }
    }

    protected function getPageTitle($separator = null)
    {
        if (isset($separator)) {
            return $this->pageTitle . " " . $separator . " ";
        } else {
            return $this->pageTitle;
        }
    }

    protected function setPageTitle($pageTitle)
    {
        $this->view['title'] = $pageTitle;
    }

}