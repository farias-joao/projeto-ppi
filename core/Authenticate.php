<?php

namespace Core;

use App\Models\User;


trait Authenticate
{
    public function login()
    {
        $this->setPageTitle("Login");
        return $this->renderView("/user/login", "base");
    }

    public function auth()
    {
        $result = User::where('email', $_POST['email'])->first();

        if (isset($result) && password_verify($_POST['password'], $result->password)) {
            $user = [
                'id' => $result->id,
                'name' => $result->name,
                'email' => $result->email,
            ];
            Session::set('user', $user);
            Session::set('sucess', ["Bem vindo ".$user['name']]);

            echo '<script>window.location = "/"</script>';
        } else {
            $error = 'Usuário ou senha estão incorretos';
            echo "<div class='alert alert-danger alert-dismissible' role='alert'>";
                echo $error;
            echo "</div>";
        }
    }

    public function logout()
    {
        Session::destroy('user');
        Session::set("sucess", ['Usuario desconectado!']);
            echo '<script>window.location = "/login"</script>';

    }
}